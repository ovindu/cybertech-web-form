# Cybertech Web Form

### About

Cybertech web form application is developed to collect user information through a simple laravel web form.

* version 1.0

### How to use

* recommending homestead
* clone the git repo
* create a database called 'cybertech_db'
* rename env.example file to env
* update database credentials in env file
* run php artisan migrate to create database tables
* seed the position table data using php artisan db:seed
* run the application

### Sending Email

* Setup a mailtrap account
* Update the email credentials in .env file
* then run php artisan config:cache

### Technologies

* Laravel Framework
* MySQL
* Material Design Bootstrap

### Issues

* Contact the owner of the repo

### License

Application licensed under the [Ovindu Shamal](http://ovindushamal.com/).
