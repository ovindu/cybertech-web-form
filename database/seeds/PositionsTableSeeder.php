<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $positions = [
            [
                'position' => 'Laraval Developers',
                'type' => 'Work Online',
                'status' => 1,
            ],
            [
                'position' => 'UI Developers',
                'type' => 'Work Online',
                'status' => 1,
            ],
            [
                'position' => 'Business Developers',
                'type' => 'Work Online',
                'status' => 1,
            ],
            [
                'position' => 'WordPress Developers',
                'type' => 'Work Online',
                'status' => 1,
            ],
            [
                'position' => 'React Native Developers',
                'type' => 'Work Online',
                'status' => 1,
            ],
            [
                'position' => 'NodeJs Developers',
                'type' => 'Work Online',
                'status' => 0,
            ],
        ];

        DB::table('positions')->insert($positions);
    }
}
