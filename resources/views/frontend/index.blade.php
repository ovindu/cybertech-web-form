@extends('layouts.app')

@section('after-styles')
    <style>
        .card {
            margin-top: 50px;
        }

        .card-header-1 {
            width: 98%;
            margin: -30px auto 15px auto;
            padding: 10px 5px 5px 5px;
            background: linear-gradient(60deg, #038DDD, #095EAE);
            -webkit-box-shadow: 0px 10px 17px -6px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 10px 17px -6px rgba(0, 0, 0, 0.75);
            box-shadow: 0px 10px 17px -6px rgba(0, 0, 0, 0.75);
        }
    </style>
@endsection

@section('content')

    @if ($errors->any())
        <div class="container text-center mt-2">
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    {!! $error !!}<br/>
                @endforeach
            </div>
        </div>
    @elseif (session()->get('flash_success'))
        <div class="container text-center mt-2">
            <div class="alert alert-success">
                @if(is_array(json_decode(session()->get('flash_success'), true)))
                    {!! implode('', session()->get('flash_success')->all(':message<br/>')) !!}
                @else
                    {!! session()->get('flash_success') !!}
                @endif
            </div>
        </div>
    @endif

    <div class="container-fluid" style="margin-top: 1%; margin-bottom: 3%;">
        <div class="card">
            <div class="card-header-1 blue">
                <h3 class="text-center text-white"><a href="https://www.facebook.com/cybertechInt.lk/"
                                                      target="_blank"><img src="/img/logo.png" alt="logo" height="100px"></a>
                    <strong>Signup
                        Form to register for Cybertech Int team</strong></h3>
            </div>

            <div class="card-body">
                <div class="container">
                    {{--intro--}}
                    <p class="text-center">Fill all below fields to complete your application. If you have arised any
                        inconvenience please call
                        <a href="tel:(+94) 71-33 99 099">(+94) 71-33 99 099</a> or email us your details to <a
                                href="mailto:lakshitha@teamcybertech.com">Apply@teamcybertech.com</a></p>
                    <hr>

                    {{--Panel--}}
                    <div class="row justify-content-center">
                        <div class="col-md-10">
                            <div class="card red lighten-4">
                                <div class="card-header text-dark text-center"><h4><strong>VACANCIES AVAILABLE TO
                                            APPLY!</strong></h4></div>
                                <div class="card-body text-primary">
                                    <ul class="list-unstyled dark-grey-text">
                                        @foreach($positions as $position)
                                            <li># <strong>{{ $position->position }}</strong> - <span
                                                        class="red-text">{{ $position->type }}</span></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    {{--Application--}}
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header blue">
                                    <h4 class="text-center text-white"><strong>Application Form</strong></h4>
                                </div>
                                <div class="card-body">
                                    {{ Form::open(['route' => 'frontend.apply.store', 'method' => 'post', 'files' => true, 'class' => 'dark-grey-text']) }}
                                    <hr class="blue">
                                    <h4 class="font-weight-bold">PERSONAL DETAILS:</h4>
                                    <hr class="blue">
                                    <label for="first_name" class="font-weight-bold">First Name <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('first_name','',['class' => 'form-control', 'id' => 'first_name', 'required' => 'required', 'placeholder' => 'Enter Your First Name']) }}
                                    <br>
                                    <label for="last_name" class="font-weight-bold">Last Name <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('last_name','',['class' => 'form-control', 'id' => 'last_name', 'required' => 'required', 'placeholder' => 'Enter Your Last Name']) }}

                                    <br>
                                    <label for="email" class="font-weight-bold">Email <span
                                                class="red-text">*</span></label>
                                    {{ Form::email('email','',['class' => 'form-control', 'id' => 'email', 'required' => 'required', 'placeholder' => 'Enter Your Email Address']) }}

                                    <br>
                                    <label for="address" class="font-weight-bold">Address <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('address','',['class' => 'form-control', 'id' => 'address', 'required' => 'required', 'placeholder' => 'Enter your Residence Address']) }}

                                    <br>
                                    <label for="mobile" class="font-weight-bold">Mobile Number <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('mobile','',['class' => 'form-control', 'id' => 'mobile', 'required' => 'required', 'placeholder' => 'Enter your Mobile Number']) }}

                                    <br>
                                    <label for="dob" class="font-weight-bold">Date of Birth <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('dob','',['class' => 'form-control datepicker', 'id' => 'dob', 'required' => 'required', 'placeholder' => 'Select Date']) }}

                                    <br>
                                    <label for="nic" class="font-weight-bold">NIC Number <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('nic','',['class' => 'form-control', 'id' => 'nic', 'required' => 'required', 'placeholder' => 'Enter your NIC Number']) }}

                                    <br>
                                    <label for="cv" class="font-weight-bold">Your CV (file size less than 2MB) <span
                                                class="red-text">*</span></label>
                                    {{ Form::file('cv',['class' => 'form-control-file', 'id' => 'cv', 'required' => 'required']) }}

                                    <br>
                                    <hr class="blue">
                                    <h4 class="font-weight-bold">PROFESSIONAL DETAILS:</h4>
                                    <hr class="blue">

                                    <label for="position" class="font-weight-bold">What is the position you need to
                                        apply?
                                        <span class="red-text">*</span></label>
                                    <select name="position" class="form-control" id="position">
                                        @foreach($positions as $position)
                                        <option value="{{ $position->position }}">{{ $position->position }}</option>
                                        @endforeach
                                    </select>

                                    <br>
                                    <label for="last_company" class="font-weight-bold">Previously worked company <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('last_company','',['class' => 'form-control', 'id' => 'last_company', 'required' => 'required', 'placeholder' => 'Enter the company name that you have been worked at last...']) }}

                                    <br>
                                    <label for="last_position" class="font-weight-bold">Your job tittle <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('last_position','',['class' => 'form-control', 'id' => 'last_position', 'required' => 'required', 'placeholder' => 'Ex: Marketing Assistant, Software Engineer, UI/UX designer, etc...']) }}

                                    <br>
                                    <label for="last_salary" class="font-weight-bold">Monthly salary was</label>
                                    {{ Form::text('last_salary','',['class' => 'form-control', 'id' => 'last_salary', 'placeholder' => 'Ex: 25000 LKR']) }}

                                    <br>
                                    <label for="experience" class="font-weight-bold">Experience in years <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('experience','',['class' => 'form-control', 'id' => 'experience', 'required' => 'required','placeholder' => 'Ex: 2 1/2 years, etc...']) }}

                                    <br>
                                    <label for="expertise_areas" class="font-weight-bold">Areas you're expertise
                                        with</label>
                                    {{ Form::textarea('expertise_areas','',['class' => 'form-control', 'id' => 'expertise_areas', 'placeholder' => 'Ex: Sales & marketing, PHP, Laraval, Java, CSS, Jscript, Bootstrap, etc...']) }}

                                    <br>
                                    <hr class="blue">
                                    <h4 class="font-weight-bold">ACCOUNT DETAILS:
                                        <small>(All payments will be deposited to this account)</small>
                                    </h4>
                                    <hr class="blue">

                                    <label for="account_number" class="font-weight-bold">Bank Account Number <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('account_number','',['class' => 'form-control', 'id' => 'account_number', 'required' => 'required', 'placeholder' => 'Enter your Account Number']) }}

                                    <br>
                                    <label for="account_name" class="font-weight-bold">Account Holder's Name <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('account_name','',['class' => 'form-control', 'id' => 'account_name', 'required' => 'required', 'placeholder' => "Enter Account holder's Name"]) }}

                                    <br>
                                    <label for="bank_name" class="font-weight-bold">Bank Name <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('bank_name','',['class' => 'form-control', 'id' => 'bank_name', 'required' => 'required', 'placeholder' => "Enter Branch Name"]) }}

                                    <br>
                                    <label for="branch" class="font-weight-bold">Branch Name <span
                                                class="red-text">*</span></label>
                                    {{ Form::text('branch','',['class' => 'form-control', 'id' => 'branch', 'required' => 'required', 'placeholder' => "Enter Bank Name"]) }}

                                    <br>
                                    {{ Form::input('checkbox','confirmed','1',['class' => '', 'id' => 'confirmed', 'required' => 'required', 'placeholder' => 'Enter your MObile Number']) }}
                                    <label for="confirmed" class="font-weight-bold">By clicking "Submit" you agree to
                                        our Terms of Use and Privacy Policy also acknowledge that data you provide are
                                        correct & accurate.<span class="red-text">*</span></label>

                                    <div class="text-center py-4 mt-3">
                                        {{ Form::button('submit', ['class' => 'btn btn-outline-green', 'type' => 'submit']) }}
                                        {{ Form::button('reset', ['class' => 'btn btn-outline-red', 'type' => 'reset']) }}
                                    </div>

                                    {{ Form::close() }}
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <div class="card-footer text-center mt-3">
                <br>
                <p>Copyright &copy; 2016-2018 @ Cybertech Int (pvt) Ltd. Powered by <a class="p-footer-a"
                                                                                       href="http://www.cybertechint.lk"
                                                                                       target="_blank">Cybertech Int</a>
                    team. Your details are End to End encrypted with this form.</p>
                <br>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')
    <script type="text/javascript">
        $(function () {

            // Date picker 1
            /* Linked date and time picker */
            // start date date and time picker
            $('.datepicker').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        });
    </script>
@endsection