@component('mail::message')
<hr>
<h3>PERSONAL DETAILS:</h3>
<hr>

<h5><strong>Name:</strong> {{ $user->first_name . ' ' . $user->last_name  }}</h5>
<h5><strong>Email:</strong> {{ $user->email  }}</h5>
<h5><strong>Address:</strong> {{ $user->address  }}</h5>
<h5><strong>Mobile Number:</strong> {{ $user->mobile  }}</h5>
<h5><strong>Date of Birth:</strong> {{ $user->dob  }}</h5>
<h5><strong>NIC Number:</strong> {{ $user->nic  }}</h5>

<br>
<hr>
<h3>PROFESSIONAL DETAILS:</h3>
<hr>

<h5><strong>Position:</strong> {{ $detail->position  }}</h5>
<h5><strong>Previously worked company:</strong> {{ $detail->last_company  }}</h5>
<h5><strong>Job tittle:</strong> {{ $detail->last_position  }}</h5>
<h5><strong>Monthly salary was:</strong> {{ $detail->last_salary  }}</h5>
<h5><strong>Experience in years:</strong> {{ $detail->experience  }}</h5>
<h5><strong>Areas you're expertise with:</strong> {{ $detail->expertise_areas  }}</h5>

<br>
<hr>
<h3>ACCOUNT DETAILS:</h3>
<hr>

<h5><strong>Bank Account Number:</strong> {{ $account->account_number  }}</h5>
<h5><strong>Account Holder's Name:</strong> {{ $account->account_name  }}</h5>
<h5><strong>Bank Name:</strong> {{ $account->bank_name  }}</h5>
<h5><strong>Branch Name:</strong> {{ $account->branch  }}</h5>

<br>

@component('mail::button', ['url' => $file])
    Download CV
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
