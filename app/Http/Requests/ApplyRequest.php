<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cv' => 'max:2000|mimes:doc,docx,pdf',
            'dob' => 'date',
        ];
    }

    public function messages()
    {
        return [
            'cv.required' => 'CV field is required.',
            'cv.max' => 'CV file must be less than 2MB.',
        ];
    }
}
